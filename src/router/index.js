import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    redirect: "/register"
  },
  {
    path: "/register",
    name: "Register",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/auth/Register.vue")
  },
  {
    path: "/dashboard",
    component: () => import("../views/dashboard/layout/MainContainer.vue"),
    redirect: "/dashboard",
    children: [
      {
        path: "/dashboard/employee",
        name: "Dashboard",
        component: () => import("../views/dashboard/Employee.vue")
      }
    ]
  },

  // all other requests redirect to dashboard
  { path: "*", redirect: "/register" }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
